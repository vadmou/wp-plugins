<?php

/**
 * Adds TEST_Favorites_Widget
 */
class TEST_Favorites_Widget extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        $args = [
            'name' => 'Избранные записи',
            'description' => 'Виджет отображает список избранных статей'
        ];
        parent::__construct('test-favorites-widget', '', $args);
    }

    /**
     * Back-end widget form.
     * @param $instance
     */
    public function form($instance)
    {
        extract($instance);
        $title = !empty($title) ? esc_attr($title) : 'Избранные записи';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title') ?>">Заголовок:</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title') ?>"
                   name="<?php echo $this->get_field_name('title') ?>"
                   type="text" value="<?php echo $title ?>">
        </p>
        <?php
    }

    /**
     * Front-end display of widget.
     * @param $args
     * @param $instance
     */
    public function widget($args, $instance)
    {
        if (!is_user_logged_in()) return;
        echo $args['before_widget'];
        echo $args['before_title'];
        echo $instance['title'];
        echo $args['after_title'];
        test_show_dashboard_widget();
        echo $args['after_widget'];

    }

    /**
     * Sanitize widget form values as they are saved.
     */
    /*
    public function update()
    {

    }
    */
}