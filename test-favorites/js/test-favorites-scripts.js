jQuery(document).ready(function ($) {

    $('.test-favorites-content a').click(function (s) {

        var action = $(this).data('action');

        $.ajax({
            type: 'POST',
            url: testFavorites.url,
            data: {
                security: testFavorites.nonce,
                action: 'test_' + action,
                postId: testFavorites.postId,
            },
            beforeSend: function () {
                $(' .test-favorites-content a').fadeOut(300, function () {
                    $('.test-favorites-content .test-favorites-loading').fadeIn();
                });
            },
            success: function (res) {
                $('.test-favorites-content .test-favorites-loading').fadeOut(300, function () {
                    $('.test-favorites-content').html(res);
                    if (action == 'delete'){
                        $('.widget_test-favorites-widget').find('li.cat-item-' + testFavorites.postId).remove();
                    }
                });
            },
            error: function () {
                alert('Ошибка ajax!');
            }
        });
        s.preventDefault();
    });
});