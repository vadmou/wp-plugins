jQuery(document).ready(function ($) {

    $('.test-favorites-delete').click(function (s) {
        s.preventDefault();
        if (!confirm('Вы действительно хотите удалить статью?')) return false;

        var post = $(this).data('post'),
            parent = $(this).parent(),
            loader = parent.next(),
            li = $(this).closest('li');

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                security: testFavorites.nonce,
                action: 'test_delete',
                postId: post
            },
            beforeSend: function () {
                parent.fadeOut(300, function () {
                    loader.fadeIn();
                });
            },
            success: function (res) {
                loader.fadeOut(300, function () {
                    li.html(res);
                });
            },
            error: function () {
                alert('Ошибка ajax!');
            }
        });
    });

    $('#test-favorites-delete-all').click(function (a) {
        a.preventDefault();
        if (!confirm('Вы действительно хотите удалить все статьи?')) return false;
        var $this = $(this),
            loader = $this.next(),
            parent = $this.parent(),
            list = parent.prev();

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                security: testFavorites.nonce,
                action: 'test_delete_all'
            },
            beforeSend: function () {
                $this.fadeOut(300, function () {
                    loader.fadeIn();
                });
            },
            success: function (res) {
                loader.fadeOut(300, function () {
                    if (res === 'Сатьи успешно удалены') {
                        parent.html(res);
                        list.fadeOut();
                    }
                    $this.fadeIn();
                    alert(res);
                });
            },
            error: function () {
                alert('Ошибка ajax!');
            }
        });
    });
});