<?php

/**
 * Add widget to console
 */

function test_favorites_dashboard_widget()
{
    wp_add_dashboard_widget('test_favorites_dashboard', 'Список Избранных записей', 'test_show_dashboard_widget');
}

/**
 * Show list of articles in widget and buttons for delete
 */

function test_show_dashboard_widget()
{
    $user = wp_get_current_user();
    $favorites = get_user_meta($user->ID, 'test_favorites');

    if (!$favorites) {
        echo 'Список пуст';
        return;
    }
    $img_src = plugins_url('/image/loading.gif', __FILE__);

    echo '<ul>';
    foreach ($favorites as $favorite) {
        echo '<li class="cat-item cat-item-'. $favorite . '">
            <a href="' . get_permalink($favorite) . '" target="_blank">' . get_the_title($favorite) . '</a>&emsp;
            <span><a href="#" data-post="' . $favorite . '" class="test-favorites-delete">&#9747;</a>
            </span><span class="test-favorites-loading"><img src="' . $img_src . '" alt=""></span>
            </li>';
    }
    echo '</ul>';
    echo '<div class="test-favorites-delete-all">
            <button class="button" id="test-favorites-delete-all">Удалить все статьи</button>
            <span class="test-favorites-loading"><img src="' . $img_src . '" alt=""></span></div>';
}

/**
 * Create and change link "Add to favorites"
 * @param $content
 * @return string
 */

function test_favorites_content($content)
{
    if (!is_single() || !is_user_logged_in()) return $content;

    global $post;
    if (test_is_favorites($post->ID)) {
        return '<p class="test-favorites-content"><span class="test-favorites-loading"><img 
        src="" alt=""></span><a data-action="delete" href="#">Удалить из Избранного</a></p>' . $content;
    }

    $img_src = plugins_url('/image/loading.gif', __FILE__);
    return '<p class="test-favorites-content"><span class="test-favorites-loading"><img 
        src="' . $img_src . '" alt=""></span><a data-action="add" href="#">Добавить в Избранное</a></p>' . $content;
}

/**
 * Connect script, styles, data in admin console
 */

function test_favorites_admin_scripts($url)
{
    if ($url != 'index.php') {
        return;
    }
    wp_enqueue_script('test-favorites-admin-scripts', plugins_url('js/test-favorites-admin-scripts.js', __FILE__),
        array('jquery'), null, true);

    wp_enqueue_style('test-favorites-admin-style', plugins_url('css/test-favorites-admin-style.css', __FILE__));

    wp_localize_script('test-favorites-admin-scripts', 'testFavorites', ['nonce' => wp_create_nonce('test-favorites')]);
}

/**
 * Connect script, styles, data in posts
 */

function test_favorites_scripts()
{
    if (!is_user_logged_in()) return;
    wp_enqueue_script('test-favorites-scripts', plugins_url('js/test-favorites-scripts.js', __FILE__),
        array('jquery'), null, true);

    wp_enqueue_style('test-favorites-style', plugins_url('css/test-favorites-style.css', __FILE__));

    global $post;
    wp_localize_script('test-favorites-scripts', 'testFavorites', ['url' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('test-favorites'), 'postId' => $post->ID]);

}

/**
 * Operations Ajax request (add articles)
 * - security key verification
 * - get articles ID and check status
 * - articles ID save
 */

function wp_ajax_test_add()
{
    if (!wp_verify_nonce($_POST['security'], 'test-favorites')) {
        wp_die('Ошибка безопасности');
    }

    $post_id = (int)$_POST['postId'];
    $user = wp_get_current_user();

    if (test_is_favorites($post_id)) {
        wp_die();
    }

    if (add_user_meta($user->ID, 'test_favorites', $post_id)) {
        wp_die('Сатья успешно добавлена');
    }
    wp_die('Ошибка добавления');
}

/**
 * Operations Ajax request (delete one article)
 */

function wp_ajax_test_delete()
{
    if (!wp_verify_nonce($_POST['security'], 'test-favorites')) {
        wp_die('Ошибка безопасности');
    }

    $post_id = (int)$_POST['postId'];
    $user = wp_get_current_user();

    if (!test_is_favorites($post_id)) {
        wp_die();
    }

    if (delete_user_meta($user->ID, 'test_favorites', $post_id)) {
        wp_die('Сатья успешно удалена');
    }
    wp_die('Ошибка удаления');
}

/**
 * Operations Ajax request (delete all articles)
 */

function wp_ajax_test_delete_all()
{
    if (!wp_verify_nonce($_POST['security'], 'test-favorites')) {
        wp_die('Ошибка безопасности');
    }
    $user = wp_get_current_user();

    if (delete_metadata('user', $user->ID, 'test_favorites')) {
        wp_die('Сатьи успешно удалены');
    }
    wp_die('Ошибка удаления');
}

/**
 *  Check ID status articles
 * @param $post_id
 * @return bool
 */

function test_is_favorites($post_id)
{
    $user = wp_get_current_user();
    $favorites = get_user_meta($user->ID, 'test_favorites');

    foreach ($favorites as $favorite) {
        if ($favorite == $post_id)
            return true;
    }
    return false;
}