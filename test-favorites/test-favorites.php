<?php
/*
Plugin Name: Избранные стратьи
Plugin URI:
Description: Дает возможность зарегестрированным пользователям добавлять статьи в раздел "Избранное".
Author: Vadym Borovok
Version: 1.0
Author URI: 
*/

/*  Copyright 2017  Vadym Borovok  (email: none)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require __DIR__ . '/functions.php';
require __DIR__ . '/TEST_Favorites_Widget.php';

add_filter('the_content', 'test_favorites_content');
add_action('wp_enqueue_scripts', 'test_favorites_scripts');

add_action('wp_ajax_test_add', 'wp_ajax_test_add');
add_action('wp_ajax_test_delete', 'wp_ajax_test_delete');
add_action ('wp_ajax_test_delete_all', 'wp_ajax_test_delete_all');

add_action('wp_dashboard_setup', 'test_favorites_dashboard_widget');
add_action ('admin_enqueue_scripts', 'test_favorites_admin_scripts');

add_action('widgets_init', 'test_favorites_widget');
function test_favorites_widget(){
    register_widget( 'TEST_Favorites_Widget');
}