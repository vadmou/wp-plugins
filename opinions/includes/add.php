<?php
include_once('models/opinions.php');
?>
<h3><a href="<?= $_SERVER['PHP_SELF'] ?>?page=opinions">Отзывы</a></h3>

<?php
if (!empty($_POST)) {
    if (opinions_add($_POST['title'], $_POST['content'])) {
        die('Отзыв успешно добавлен!');
    }

    $title = $_POST['title'];
    $content = $_POST['content'];
    $error = true;
} else {
    $title = '';
    $content = '';
    $error = false;
}
?>

<h2>Новый отзыв</h2>
<? if ($error): ?>
    <p>Пожалуйста, заполните все поля!</p>
<? endif; ?>
<form method="post">
    Имя Фамилия:
    <br/>
    <input type="text" name="title" value="<?= $title ?>"/>
    <br/>
    <br/>
    Содержание:
    <br/>
    <textarea name="content"><?= $content ?></textarea>
    <br/>
    <input type="submit" value="Добавить"/>
</form>