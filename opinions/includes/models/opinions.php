<?php
function opinions_all()
{
    global $wpdb;
    $table = $wpdb->prefix . 'opinions';
    $query = "SELECT id_opinion, name, text FROM $table ORDER BY id_opinion DESC";
    return $wpdb->get_results($query, ARRAY_A);
}

function opinions_get($id_opinion)
{
    global $wpdb;
    $table = $wpdb->prefix . 'opinions';
    $t = "SELECT name, text FROM $table WHERE id_opinion='%d'";
    $query = $wpdb->prepare($t, $id_opinion);
    return $wpdb->get_row($query, ARRAY_A);
}

function opinions_add($title, $content)
{
    global $wpdb;

    $title = trim($title);
    $content = trim($content);

    if ($title == '' || $content == '')
        return false;

    $table = $wpdb->prefix . 'opinions';
    $t = "INSERT INTO $table (name, text) VALUES('%s', '%s')";
    $query = $wpdb->prepare($t, $title, $content);
    $result = $wpdb->query($query);

    if ($result === false)
        die('Ошибка БД');

    return true;
}

function opinions_edit($id_opinion, $title, $content)
{
    global $wpdb;

    $title = trim($title);
    $content = trim($content);

    if ($title == '' || $content == '')
        return false;

    $table = $wpdb->prefix . 'opinions';
    $t = "UPDATE $table SET name='%s', text='%s' WHERE id_opinion='%d'";
    $query = $wpdb->prepare($t, $title, $content, $id_opinion);
    $result = $wpdb->query($query);

    if ($result === false)
        die('Ошибка БД');

    return true;
}

function opinions_delete($id_opinion)
{
    global $wpdb;
    $table = $wpdb->prefix . 'opinions';
    $t = "DELETE FROM $table WHERE id_opinion='%d'";
    $query = $wpdb->prepare($t, $id_opinion);
    return $wpdb->query($query);
}