<?php
include_once('models/opinions.php');
$opinions = opinions_all();
?>

<div id="opinions">
    <? foreach ($opinions as $opinion): ?>

        <!-- start short version -->
        <div style="word-wrap: break-word; width: 100%; border-top: 1px dotted #555;">
            <div style="font: bold 14px Arial; padding: 10px 0px; text-align: center; overflow: hidden;"><?= $opinion['name'] ?></div>
            <div><?= $opinion['text'] ?></div>
            </li>
            <div>Читать отзыв полностью</div>
        </div>
        <!-- end short version -->

    <? endforeach ?>
</div>
