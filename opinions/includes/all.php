<?php
include_once('models/opinions.php');
$opinions = opinions_all();
?>

<h2>Отзывы</h2>
<ul>
    <li><a href="<?= $_SERVER['PHP_SELF'] ?>?page=opinions&c=add">Добавить новый отзыв</a></li>
    <? foreach ($opinions as $op): ?>
        <li>
            <a href="<?= $_SERVER['PHP_SELF'] ?>?page=opinions&c=edit&id=<?= $op['id_opinion'] ?>"><?= $op['name'] ?></a>
        </li>
    <? endforeach; ?>
</ul>