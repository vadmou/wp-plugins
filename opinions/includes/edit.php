<?php
include_once('models/opinions.php');
?>

<h3><a href="<?= $_SERVER['PHP_SELF'] ?>?page=opinions">Отзывы</a></h3>

<?php
$id = (int)$_GET['id'];

if ($id == 0)
    die('Не передан id отзыва!');

if (!empty($_POST)) {
    if (isset($_POST['save'])) { // если нажата кнопка save
        if (opinions_edit($id, $_POST['title'], $_POST['content'])) {
            die('Отзыв успешно отредактирован!');
        }
    } elseif (isset($_POST['delete'])) {
        die('Отзыв успешно удалён!');
    }


    $title = $_POST['title'];
    $content = $_POST['content'];
    $error = true;
} else {
    $opinion = opinions_get($id);
    $title = $opinion['name'];
    $content = $opinion['text'];
    $error = false;
}
?>

<h2>Редактирование отзыв</h2>

<? if ($error): ?>
    <p>Пожалуйста, заполните все поля!</p>
<? endif; ?>

<form method="post">
    Имя Фамилия:
    <br/>
    <input type="text" name="title" value="<?= $title ?>"/>
    <br/>
    <br/>
    Содержание:
    <br/>
    <textarea name="content"><?= $content ?></textarea>
    <br/>
    <input type="submit" name="save" value="Сохранить"/>
    <input type="submit" name="delete" value="Удалить"/>
</form>