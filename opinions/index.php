<?php
/*
Plugin Name: Отзывы
Plugin URI:
Description: Возможность добавлять отзывы.
Author: Vadym Borovok
Version: 1.0
Author URI:
*/

/*  Copyright 2017  Vadym Borovok  (email: none)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function opinions_install()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'opinions';

    if ($wpdb->get_var("SHOW TABLES LIKE $table_name") != $table_name) {
        $sql = "CREATE TABLE IF NOT EXISTS `$table_name` ( 
					`id_opinion` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(40) NOT NULL,
					`text` text NOT NULL,
					PRIMARY KEY (`id_opinion`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        $wpdb->query($sql);
    }

    add_option('opinions_on_page', 5);
}

function opinions_uninstall()
{
    delete_option('opinions_on_page');
}

function opinions_ondelete()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'opinions';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
}

// Хуки
register_activation_hook(__FILE__, 'opinions_install');
register_deactivation_hook(__FILE__, 'opinions_uninstall');
register_uninstall_hook(__FILE__, 'opinions_ondelete');

function opinions_admin_menu()
{
    add_posts_page('Отзывы', 'Отзывы', 8, 'opinions', 'opinions_editor');
}

add_action('admin_menu', 'opinions_admin_menu');

function opinions_editor()
{

    switch ($_GET['c']) {
        case 'add':
            $action = 'add';
            break;
        case 'edit':
            $action = 'edit';
            break;
        default:
            $action = 'all';
            break;
    }

    include_once("includes/$action.php");
}

function opinions_short()
{
    ob_start();
    include_once("includes/intro.php");
    return ob_get_clean();
}

add_filter('widget_text', 'do_shortcode');
add_shortcode('opinions', 'opinions_short');